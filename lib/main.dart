import 'package:flutter/material.dart';
void main(){
  runApp(MaterialApp(
    title: 'task_4',
    home: home(),
    debugShowCheckedModeBanner: false,
  ));
}
class home extends StatefulWidget {
  @override
  _homeState createState() => _homeState();
}

class _homeState extends State<home> {
  Widget _rows(String txt,Color concolors,IconData icon,Color iconcolor){
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Padding(padding: EdgeInsets.only(right: 10,top: 15),child: Text(txt,style: TextStyle(fontWeight: FontWeight.w800,
          fontSize: 17
        ),),),
      Padding(
        padding: EdgeInsets.only(top: 10,right: 10),
        child:  Container(
          width: 45,
          height: 45,
          decoration: BoxDecoration(
            color: concolors,
            borderRadius: BorderRadius.circular(5)
          ),
          child: Center(
            child: Icon(icon,color: iconcolor,),
          ),
        ),
      )
      ],
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text('حسابي',style: TextStyle(color: Colors.black),),
        actions: <Widget>[
          Padding(padding: EdgeInsets.only(right: 10),child: Icon(Icons.arrow_forward_ios,color: Colors.black,),)
        ],
      ),
      body: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 20,right: 20,top: 20),
            child:Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5)
              ),
              height: MediaQuery.of(context).size.height/3-110,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left:70,top: 40),
                    child: Column(
                      children: <Widget>[
                      Text('(جيمي)جمال عبدالناصر',style: TextStyle(fontSize: 18),),
                       Text('+534853485348')

                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left:20,right: 10),child: Image.asset('assets/gemy.jpg',width: 120,),
                  ),
                ],
              ),
            ) ,
          ),
          Padding(
            padding: EdgeInsets.only(left: 20,right: 20,top: 20),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Colors.white
              ),
              height: MediaQuery.of(context).size.height/3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                   _rows('تعديل الحساب',Colors.pink[100], Icons.settings,Colors.pink),
                  _rows('التقييمات', Colors.green[100], Icons.star, Colors.green),
                  _rows('الشكاوي',Colors.blue[100], Icons.send, Colors.blue),
                  _rows('الأسئلة الشائعة', Colors.brown[100], Icons.assignment, Colors.brown)
                ],
              ),
            ),
          ),
          Padding(padding: EdgeInsets.only(left: 20,right: 20,top:20),
            child:Container(
              height: MediaQuery.of(context).size.height/4,
              decoration: BoxDecoration(
                color: Colors.white
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  _rows('تواصل معنا', Colors.deepPurple[100], Icons.headset_mic, Colors.purple),
                  _rows('عن التطبيق', Colors.purple[100], Icons.info_outline, Colors.purple),
                  _rows('شارك التطبيق', Colors.cyan[100], Icons.share, Colors.cyan)
                ],
              ),
            )
          )
        ],
      ),
    );
  }
}
